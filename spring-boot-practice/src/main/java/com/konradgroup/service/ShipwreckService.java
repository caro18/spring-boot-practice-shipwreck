package com.konradgroup.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konradgroup.model.Shipwreck;
import com.konradgroup.repository.ShipwreckRepository;
@Service
public class ShipwreckService {
	
	private ShipwreckRepository shipwreckRepository;
	
	@Autowired
	public ShipwreckService(ShipwreckRepository shipwreckRepository) {
		this.shipwreckRepository = shipwreckRepository;
	}
	public Shipwreck createShipwreck(Shipwreck shipwreck) {
		return shipwreckRepository.saveAndFlush(shipwreck);
	}
	public Shipwreck delete(Long id) {
		Shipwreck existingS =shipwreckRepository.findOne(id);
		shipwreckRepository.delete(existingS);
		return existingS;
	}
	public Shipwreck save(Long id, Shipwreck shipwreck) {
		Shipwreck existingS =shipwreckRepository.findOne(id);
		BeanUtils.copyProperties(shipwreck, existingS);
		return  shipwreckRepository.save(existingS);
	}
	public Shipwreck findOne(Long id) {
		return shipwreckRepository.findOne(id);
	}
	public List<Shipwreck> findAll() {
		return shipwreckRepository.findAll();
	}

}
