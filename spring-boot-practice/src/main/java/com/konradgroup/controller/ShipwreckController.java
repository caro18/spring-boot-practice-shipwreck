package com.konradgroup.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.konradgroup.model.Shipwreck;
import com.konradgroup.service.ShipwreckService;

@RestController
@RequestMapping("api/v1")
public class ShipwreckController {

	private ShipwreckService shipwreckService;
	
	@Autowired
	public ShipwreckController(ShipwreckService shipwreckService) {
		this.shipwreckService = shipwreckService;

	}
	
	public ShipwreckController() {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(value ="shipwrecks", method = RequestMethod.GET)
	
	public List<Shipwreck> list() {
		return shipwreckService.findAll();

	}
	//POST 
	@RequestMapping(value ="shipwrecks", method = RequestMethod.POST)
	public Shipwreck create(@RequestBody Shipwreck shipwreck) {
		System.out.println("POST!!!!!!!!!");
		System.out.println(shipwreck);
		return shipwreckService.createShipwreck(shipwreck);
	}
	
	@RequestMapping(value ="shipwrecks/{id}", method = RequestMethod.GET)
	public Shipwreck get(@PathVariable Long id) {
		return shipwreckService.findOne(id);

	}
	
	@RequestMapping(value ="shipwrecks/{id}", method = RequestMethod.PUT )
	public Shipwreck update(@PathVariable Long id, @RequestBody Shipwreck shipwreck) {
		return  shipwreckService.save(id,shipwreck);

	}
	
	@RequestMapping(value ="shipwrecks/{id}", method = RequestMethod.DELETE)
	public Shipwreck delete(@PathVariable Long id) {
		return  shipwreckService.delete(id);

	}

}
