package com.konradgroup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.konradgroup.model.Shipwreck;

@Repository
public interface ShipwreckRepository extends JpaRepository<Shipwreck, Long> {

}
