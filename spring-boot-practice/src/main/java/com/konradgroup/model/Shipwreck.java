package  com.konradgroup.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SHIPWRECK")
public class Shipwreck {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	private String description;
	private String condition;
	private int depth;
//	Double latitude;
	private double longitude;
	private int yearDiscovered;

	public Shipwreck() { }

	public Shipwreck(Long id, String name, String description, String condition, int depth,  double longitude, int yearDiscovered) {//Double latitude,
		this.id = id;
		this.name = name;
		this.description = description;
		this.condition = condition;
		this.depth = depth;
//		this.latitude = latitude;
		this.longitude = longitude;
		this.yearDiscovered = yearDiscovered;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

//	public Double getLatitude() {
//		return latitude;
//	}
//
//	public void setLatitude(Double latitude) {
//		this.latitude = latitude;
//	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getYearDiscovered() {
		return yearDiscovered;
	}

	public void setYearDiscovered(int yearDiscovered) {
		this.yearDiscovered = yearDiscovered;
	}

	@Override
	public String toString() {
		return "Shipwreck [id=" + id + ", name=" + name + ", description=" + description + ", condition=" + condition
				+ ", depth=" + depth + ", longitude=" + longitude + ", yearDiscovered=" + yearDiscovered + "]";
	}
}
