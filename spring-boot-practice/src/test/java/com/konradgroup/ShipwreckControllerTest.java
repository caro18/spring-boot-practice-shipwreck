package com.konradgroup;

import static org.junit.Assert.*;

import org.junit.Test;

import com.konradgroup.controller.ShipwreckController;
import com.konradgroup.model.Shipwreck;

public class ShipwreckControllerTest {
	@Test
	public void testShipwreckGet(){
		ShipwreckController sc = new ShipwreckController();
		Shipwreck wreck = sc.get(1L);
		assertEquals(1l, wreck.getId().longValue());
	}

}
